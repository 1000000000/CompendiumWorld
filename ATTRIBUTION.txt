ATTRIBUTION.TXT

Adapted from material in Dungeon World by Sage LaTorra and Adam Koebel:
	Bearer (from Fighter class)
	Cleric (from Cleric class)
	Minstrel (from Bard class)
	Paladin (from Paladin class)
	Poisoner (from Thief class)
	Skinchanger (from Druid class)
	Whisperer (from Druid class)
	Wizard (from Wizard class)

Adapted from material in Class Warfare by Red Box Vancouver:
	Storyteller (from Bard compendium class)

Adaoted from material in The Perilous Wilds by Lampblack & Brimstone:
	Hunter (from Hunter class)

Adapted from material in Freebooters on the Frontier by Lampblack & Brimstone:
	Rogue (from Thief class)
	Luck mechanics

Adapted from material in Dungeon World Advanced (v0.2):
    Druid (from Druid class)

Additional attributions contained in individual .tex files.